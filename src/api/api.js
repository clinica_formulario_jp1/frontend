export const nuevaEncuesta = async (data) => {
  try {
    console.log(JSON.stringify({data:data}))
    const response = await fetch('http://localhost:8000/api/encuesta', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify([data]),
    });

    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      throw new Error('Credenciales inválidas');
    }
  } catch (error) {
    throw new Error('Error al iniciar sesión');
  }
};
export const getUsers = async () => {
  try {
    const response = await fetch('http://localhost:8000/api/users', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (!response.ok) {
      throw new Error('Error al obtener los datos de usuario.');
    }

    const data = await response.json(); 
    return data; 
  } catch (error) {
    throw new Error('Error al obtener los datos de usuario.');
  }
};
export const getEncuestaId = async (id) => {
  try {
    const response = await fetch(`http://localhost:8000/api/encuesta/find-by-id/${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (!response.ok) {
      throw new Error('Error al obtener los datos de usuario.');
    }

    const data = await response.json(); 
    return data; 
  } catch (error) {
    throw new Error('Error al obtener los datos de usuario.');
  }
};
export const deleteEncuesta = async (id) => {
  try {
    const response = await fetch(`http://localhost:8000/api/encuesta/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      return response;
    } else {
      throw new Error('Error al Eliminar la publicación');
    }
  } catch (error) {
    throw new Error('Error al Eliminar la publicación');
  }
};
