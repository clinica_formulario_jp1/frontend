import React, { useState, useEffect, useRef } from 'react';
import { Collapse, Button, message, Space } from 'antd';
import { useParams } from 'react-router-dom';
import { nuevaEncuesta, getEncuestaId } from '../../api/api';
import { useNavigate } from 'react-router-dom';
// Importa tus componentes de formulario aquí
import FormEncuestaCardioVascularDirigida from './components/FormEncuestaCardiovascularDirigida';
import CuestionarioMinimentalTest from './components/FormCuestionarioMinimalTest';
import TestRecordatorioForm from './components/FormTestRecordatorio';
import MyFoodFrequencyForm from './components/FormFrecuenciaConsumo';
import MyHealthQuestionnaire from './components/FormCuestionarioSalud';
import SleepScale from './components/FormEscalaSueno';
import EdmontonScale from './components/FormEdmonton';
import RosenbergSelfEsteemForm from './components/FromRosenberg';
import ElectrocardiogramForm from './components/FormElectroCardiograma';
import ParametrosAntropometricosForm from './components/FormParametrosAntropometricos';
import EcocardiogramaDopplerColorForm from './components/FormEcoDoppler';
import EcocardiogramaVasosCuelloForm from './components/FormEcoCardiogramaVasosCuello';
import EjercicioForm from './components/FormEstadoFisico';
import FichaDevolucionPacienteForm from './components/FormFichaDevolucion';

const { Panel } = Collapse;

function EncuestaPage() {
  const [openPanels, setOpenPanels] = useState([]);
  const [completedFormsData, setCompletedFormsData] = useState({});
  const { id } = useParams();
  const navigate = useNavigate();
  const [formData, setFormData] = useState({});
  const [missingFormData, setMissingFormData] = useState([]);
  const formRefs = {};
  const formNames = [
    'ENCUESTA CARDIOVASCULAR DIRIGIDA',
    'CUESTIONARIO MINIMENTAL TEST',
    'TEST RECORDATORIO DE 24 HS',
    'CUESTIONARIO DE FRECUENCIA DE CONSUMO DE ALIMENTOS',
    'CUESTIONARIO DE SALUD SF12',
    'ESCALA DE SUEÑO',
    'ESCALA EDMONTON',
    'ESCALA ROSENBERG',
    'ELECTRO CARDIOGRAMA',
    'PARÁMETROS ANTROPOMÉTRICOS',
    'ECOCARDIOGRAMA DOPPLER COLOR',
    'ECODOPPLER VASCULAR PERIFERICO',
    'ESTADO FISICO',
    'FICHA DEVOLUCION PACIENTE'
  ];

  useEffect(() => {
    if (id !== undefined) {
      loadData(id);
    }
  }, []);


  const loadData = async (id) => {
    try {
      const response = await getEncuestaId(id);
      setFormData(response);
    } catch (error) {
      console.error('Error al obtener usuarios:', error);
    }
  };

  const newEncuesta = async (formData) => {
    try {
      const response = await nuevaEncuesta(formData);
      message.success('Formularios enviados correctamente');
    } catch (error) {
      console.error('Error al crear la nueva encuesta:', error);
    }
  };
  const formatFormName = (formName) => {
      const formattedName = formName.replace(/\s+/g, '_');
      return formattedName;
    };
  const handleCollectData = async () => {
    const missingForms = [];
    const collectedData = {};

    const validationPromises = formNames.map(async (formName) => {
      const formRef = formRefs[formName];
      
      if (formRef) {
        try {
          await formRef.validateFields();
          const formValue = formRef.getFieldsValue();
          console.log(`Datos recopilados para ${formName}:`, formValue);

          const formattedFormName = formatFormName(formName);
          collectedData[formattedFormName] = formValue;
        } catch (errorInfo) {
          console.error(`Campos no válidos en ${formName}:`, errorInfo);
          missingForms.push(formName);
        }
      }
    });

    await Promise.all(validationPromises);
     setCompletedFormsData((prevData) => ({
        ...prevData,
        ...collectedData,
      }));
    const numElements = Object.keys(completedFormsData).length;
    if (numElements === formNames.length) {
      message.success('Formularios enviados correctamente');
      newEncuesta(completedFormsData)

    } else {
      message.error('Falta completar algunos formularios antes de enviar.');
      setMissingFormData(missingForms);
    }
  };


  const handleGoBack = () => {
    navigate(-1);
  };

  const getFormComponent = (formName) => {
    switch (formName) {
      case 'ENCUESTA CARDIOVASCULAR DIRIGIDA':
        return (
          <FormEncuestaCardioVascularDirigida
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ENCUESTA_CARDIOVASCULAR_DIRIGIDA}
          />
        );
      case 'CUESTIONARIO MINIMENTAL TEST':
        return (
          <CuestionarioMinimentalTest
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.CUESTIONARIO_MINIMENTAL_TEST}
          />
        );
      case 'TEST RECORDATORIO DE 24 HS':
        return (
          <TestRecordatorioForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.TEST_RECORDATORIO_DE_24_HS}
          />
        );
      case 'CUESTIONARIO DE FRECUENCIA DE CONSUMO DE ALIMENTOS':
        return (
          <MyFoodFrequencyForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.CUESTIONARIO_DE_FRECUENCIA_DE_CONSUMO_DE_ALIMENTOS}
          />
        );
      case 'CUESTIONARIO DE SALUD SF12':
        return (
          <MyHealthQuestionnaire
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.CUESTIONARIO_DE_SALUD_SF12}
          />
        );
      case 'ESCALA DE SUEÑO':
        return (
          <SleepScale
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ESCALA_DE_SUEÑO}
          />
        );
      case 'ESCALA EDMONTON':
        return (
          <EdmontonScale
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ESCALA_EDMONTON}
          />
        );
      case 'ESCALA ROSENBERG':
        return (
          <RosenbergSelfEsteemForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ESCALA_ROSENBERG}
          />
        );
      case 'ELECTRO CARDIOGRAMA':
        return (
          <ElectrocardiogramForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ELECTRO_CARDIOGRAMA}
          />
        );
      case 'PARÁMETROS ANTROPOMÉTRICOS':
        return (
          <ParametrosAntropometricosForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.PARÁMETROS_ANTROPOMÉTRICOS}
          />
        );
      case 'ECOCARDIOGRAMA DOPPLER COLOR':
        return (
          <EcocardiogramaDopplerColorForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ECOCARDIOGRAMA_DOPPLER_COLOR}
          />
        );
      case 'ECODOPPLER VASCULAR PERIFERICO':
        return (
          <EcocardiogramaVasosCuelloForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ECODOPPLER_VASCULAR_PERIFERICO}
          />
        );
      case 'ESTADO FISICO':
        return (
          <EjercicioForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.ESTADO_FISICO}
          />
        );
      case 'FICHA DEVOLUCION PACIENTE':
        return (
          <FichaDevolucionPacienteForm
            ref={(ref) => (formRefs[formName] = ref)}
            formData={formData.FICHA_DEVOLUCION_PACIENTE}
          />
        );
      default:
        return null;
    }
  };
  return (
    <div>
      {id ? (
        <>
        <Button onClick={handleGoBack} type="primary">
          Volver Atrás
        </Button>
        <h1>Editar Encuesta</h1>
      </>
      ):
      <h1>Nueva Encuesta</h1>
      }
      <Collapse accordion activeKey={openPanels} onChange={(keys) => setOpenPanels(keys)} style={{ transitionDuration: '0.001s' }}>
        {formNames.map((formName, index) => (
          <Panel header={formName} key={String(index)} forceRender={true}>
            {getFormComponent(formName)}
          </Panel>
        ))}
      </Collapse>
      <Space>
        <Button onClick={handleCollectData} type="primary">
          Recopilar Datos
        </Button>
      </Space>
      {missingFormData.length > 0 && (
        <div>
          <h3>Falta información en los siguientes Encuestas:</h3>
          <ul>
            {missingFormData.map((formName) => (
              <li key={formName}>{formName}</li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default EncuestaPage;