import React, { useState ,forwardRef,useImperativeHandle, useEffect} from 'react';
import { Form, Input, Button } from 'antd';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';

const TestRecordatorioForm = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [form] = Form.useForm();
  const [comidas, setComidas] = useState([
    { seccion: 'DESAYUNO', nombre: '', unidades: '', nadaOtros: '' },
    { seccion: 'MEDIA MAÑANA', nombre: '', unidades: '', nadaOtros: '' },
    { seccion: 'ALMUERZO (recordar postres)', nombre: '', unidades: '', nadaOtros: '' },
    { seccion: 'MERIENDA', nombre: '', unidades: '', nadaOtros: '' },
    { seccion: 'MEDIA TARDE', nombre: '', unidades: '', nadaOtros: '' },
    { seccion: 'CENA (recordar postres)', nombre: '', unidades: '', nadaOtros: '' },
  ]);

  const handleAddComida = (seccion) => {
    setComidas([...comidas, { seccion, nombre: '', unidades: '', nadaOtros: '' }]);
  };

  const handleRemoveComida = (index) => {
    const updatedComidas = [...comidas];
    updatedComidas.splice(index, 1);
    setComidas(updatedComidas);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const updatedComidas = [...comidas];
    updatedComidas[index][name] = value;
    setComidas(updatedComidas);
  };

  const getFieldsValue = () => {
    return comidas;
  };
  const handleFormSubmit = async () => {
    try {
      const values = await form.validateFields();
      return values
    } catch (errorInfo) {
      console.error('Error de validación:', errorInfo);
    }
  };

   useImperativeHandle(ref, () => ({
    getFieldsValue,
    validateFields: handleFormSubmit,
  }));

  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      {comidas.map((comida, index) => (
        <div key={index}>
          <h3>{comida.seccion}</h3>
          <Form.Item label={`Nombre`} name={`Nombre`}>
            <Input
              name="nombre"
              value={comida.nombre}
              onChange={(e) => handleInputChange(e, index)}
              placeholder="Té, Mate, leche, etc."
            />
          </Form.Item>
          <Form.Item label="Colocar unidades" name={`unidades`}>
            <Input
              name="unidades"
              value={comida.unidades}
              onChange={(e) => handleInputChange(e, index)}
              placeholder="Unidades"
            />
          </Form.Item>
          <Form.Item label="Nada / Otros" name={`nadaOtros`}>
            <Input
              name="nadaOtros"
              value={comida.nadaOtros}
              onChange={(e) => handleInputChange(e, index)}
              placeholder="Nada, Otros, etc."
            />
          </Form.Item>
          <Button
            type="danger"
            icon={<MinusCircleOutlined />}
            onClick={() => handleRemoveComida(index)}
          >
            Eliminar Comida
          </Button>
        </div>
      ))}
      <Form.Item>
        <Button
          type="dashed"
          onClick={() => handleAddComida('DESAYUNO')}
          icon={<PlusOutlined />}
        >
          Agregar DESAYUNO
        </Button>
        <Button
          type="dashed"
          onClick={() => handleAddComida('MEDIA MAÑANA')}
          icon={<PlusOutlined />}
        >
          Agregar MEDIA MAÑANA
        </Button>
        <Button
          type="dashed"
          onClick={() => handleAddComida('ALMUERZO (recordar postres)')}
          icon={<PlusOutlined />}
        >
          Agregar ALMUERZO (recordar postres)
        </Button>
        <Button
          type="dashed"
          onClick={() => handleAddComida('MERIENDA')}
          icon={<PlusOutlined />}
        >
          Agregar MERIENDA
        </Button>
        <Button
          type="dashed"
          onClick={() => handleAddComida('MEDIA TARDE')}
          icon={<PlusOutlined />}
        >
          Agregar MEDIA TARDE
        </Button>
        <Button
          type="dashed"
          onClick={() => handleAddComida('CENA (recordar postres)')}
          icon={<PlusOutlined />}
        >
          Agregar CENA (recordar postres)
        </Button>
      </Form.Item>
    </Form>
  );
});

export default TestRecordatorioForm;
