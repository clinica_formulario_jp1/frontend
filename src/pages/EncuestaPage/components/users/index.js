import React, { useEffect, useState } from "react";
import { Table, Button, Space } from "antd";
import { Link } from "react-router-dom";
import { deleteEncuesta, getUsers } from "../../../../api/api";

const ListUser = () => {
  const [users, setUsers] = useState([]);
  const columns = [
    {
      title: "Encuesta",
      dataIndex: 'id_informe',
      key: "id_informe",
    },
    {
      title: "Nombre",
      dataIndex: ["ENCUESTA_CARDIOVASCULAR_DIRIGIDA", "nombreApellido"],
      key: "nombre",
    },
    {
      title: "Edad",
      dataIndex: ["ENCUESTA_CARDIOVASCULAR_DIRIGIDA", "edad"],
      key: "edad",
    },
    {
      title: "Fecha Nacimiento",
      dataIndex: ["ENCUESTA_CARDIOVASCULAR_DIRIGIDA", "fechaNacimiento"],
      key: "fechaNacimiento",
    },
    {
      title: "DNI",
      dataIndex: ["ENCUESTA_CARDIOVASCULAR_DIRIGIDA", "dni"],
      key: "dni",
    },
    {
      title: "Acciones",
      key: "acciones",
      render: (text, record) => (
        <Space size="middle">
          <Link to={`/encuesta/${record.id_informe}`}>Editar</Link>
          <Button onClick={() => handleEliminar(record.id_informe)}>Eliminar</Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      const response = await getUsers();
      setUsers(response);
    } catch (error) {
      console.error('Error al obtener usuarios:', error);
    }
  };
  const deleteUserEncuesta = async (id) => {
    try {
      const response = await deleteEncuesta(id);
      fetchUsers();
    } catch (error) {
      console.error('Error al obtener usuarios:', error);
    }
  };


  const handleEliminar = (id) => {
    deleteUserEncuesta(id)

  };

  return (
    <Table
      dataSource={users}
      columns={columns}
      rowKey={(record) => record.id_informe}
    />
  );
};

export default ListUser;
