import React, { forwardRef, useEffect } from 'react';
import { Input, Form, InputNumber, Select, DatePicker, Button } from 'antd';
const { Option } = Select;

const CuestionarioMinimentalTest = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <table>
        <tbody> {/* Agregar <tbody> aquí */}
          {/* Pregunta 1 */}
          <tr>
            <td>1.- Por favor, dígame la fecha de hoy.</td>
            <td>
              <Form.Item
                name="pregunta1"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <Input type="date" />
              </Form.Item>
            </td>
          </tr>

          {/* Pregunta 2 */}
          <tr>
            <td>
              2.- Ahora le voy a nombrar tres objetos. Después que se los diga,
              le voy a pedir que repita en voz alta los que recuerde, en cualquier orden.
              <br />
              Recuerde los objetos porque se los voy a preguntar más adelante.
            </td>
            <td>
              <Form.Item
                name="pregunta2"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <Input type="text" />
              </Form.Item>
            </td>
          </tr>

          {/* Resto de las preguntas */}
          {/* ... */}

          {/* Pregunta 6 */}
          <tr>
            <td>
              6.- Por favor copie este dibujo.
              <br />
              Muestre al entrevistado el dibujo con los dos pentágonos que se cruzan.
              La acción esta correcta si los dos pentágonos se cruzan y forman un cuadrilátero.
            </td>
            <td>
              <Form.Item
                name="pregunta6"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <Select style={{ width: '100%' }}>
                  <Option value="Correcto">Correcto</Option>
                  <Option value="Incorrecto">Incorrecto</Option>
                </Select>
              </Form.Item>
            </td>
          </tr>

          {/* Suma Total */}
          <tr>
            <td>Suma total (puntuación máxima: 19 puntos)</td>
            <td>
              <Form.Item
                name="sumaTotal"
                rules={[{ required: true, message: 'Campo requerido' }]}
              >
                <InputNumber style={{ width: '100%' }} />
              </Form.Item>
            </td>
          </tr>
        </tbody> 
      </table>
    </Form>
  );
});

export default CuestionarioMinimentalTest;
