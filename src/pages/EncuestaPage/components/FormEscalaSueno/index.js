import React, { useState ,forwardRef, useEffect} from 'react';
import { Form, Input, Radio, Select, Checkbox, Button, Row, Col } from 'antd';

const { Option } = Select;

const SleepScale = forwardRef(({ onFormSubmit }, ref) => {
  const [formData, setFormData] = useState({
    bedtime: '',
    timeToFallAsleep: '',
    wakeUpTime: '',
    actualSleepHours: '',
    sleepProblems: {
      troubleFallingAsleep: '',
      wakingUpDuringNight: '',
      needingToUseRestroom: '',
      difficultyBreathing: '',
      loudSnoring: '',
      feelingCold: '',
      feelingHot: '',
      nightmares: '',
      experiencingPain: '',
      otherReasons:'', 
      otherReasonsCheck: '', 
    },
    sleepQuality: '',
    medicationFrequency: '',
    daytimeDrowsinessFrequency: '',
    impactOnActivities: '',
    sleepCompanionship: '',
  });

  const handleChange = (field, value) => {
    setFormData({ ...formData, [field]: value });
  };

  const handleSleepProblemsChange = (field, value) => {
    setFormData({
      ...formData,
      sleepProblems: { ...formData.sleepProblems, [field]: value },
    });
  };

  const handleSubmit = (value) => {
    onFormSubmit(formData);
  };

  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormDataForm(formData);
      }
    }, [formData]);
  const setFormDataForm = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <Form.Item name ='slepTime1' label="1. Durante el último mes, ¿cuál ha sido, normalmente, su hora de acostarse?">
        <Input
          placeholder="___:___"
          value={formData.bedtime}
          onChange={(e) => handleChange('bedtime', e.target.value)}
        />
      </Form.Item>

      <Form.Item name ='slepTime2' label="2. ¿Cuánto tiempo habrá tardado en dormirse, normalmente, las noches del último mes?">
        <Radio.Group
          value={formData.timeToFallAsleep}
          onChange={(e) => handleChange('timeToFallAsleep', e.target.value)}
        >
          <Radio value="Menos de 15 min">Menos de 15 min</Radio>
          <Radio value="Entre 16-30 min">Entre 16-30 min</Radio>
          <Radio value="Entre 31-60 min">Entre 31-60 min</Radio>
          <Radio value="Más de 60 min">Más de 60 min</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name ='slepTime3' label="3. Durante el último mes, ¿a qué hora se ha levantado habitualmente por la mañana?">
        <Input
          placeholder="___:___"
          value={formData.wakeUpTime}
          onChange={(e) => handleChange('wakeUpTime', e.target.value)}
        />
      </Form.Item>

      <Form.Item name ='slepTime4' label="4. ¿Cuántas horas calcula que habrá dormido verdaderamente cada noche durante el último mes? El tiempo puede ser diferente al que usted permanezca en la cama.">
        <Input
          placeholder="_____"
          value={formData.actualSleepHours}
          onChange={(e) => handleChange('actualSleepHours', e.target.value)}
        />
      </Form.Item>

      <Form.Item name ='slepTime5' label="5. Durante el último mes, cuántas veces ha tenido usted problemas para dormir a causa de:">
        <Checkbox.Group
          style={{ width: '100%' }}
          value={Object.keys(formData.sleepProblems).filter(
            (problem) => formData.sleepProblems[problem]
          )}
          onChange={(values) =>
            setFormData({
              ...formData,
              sleepProblems: {
                ...formData.sleepProblems,
                ...Object.fromEntries(
                  Object.keys(formData.sleepProblems).map((problem) => [
                    problem,
                    values.includes(problem),
                  ])
                ),
              },
            })
          }
        >
          <Row>
            <Col span={12}>
              <Checkbox value="troubleFallingAsleep">No poder conciliar el sueño en la primera media hora</Checkbox>
              <Checkbox value="wakingUpDuringNight">Despertarse durante la noche o de madrugada</Checkbox>
              <Checkbox value="needingToUseRestroom">Tener que levantarse para ir al servicio</Checkbox>
              <Checkbox value="difficultyBreathing">No poder respirar bien</Checkbox>
              <Checkbox value="loudSnoring">Toser o roncar ruidosamente</Checkbox>
              <Checkbox value="feelingCold">Sentir frío</Checkbox>
            </Col>
            <Col span={12}>
              <Checkbox value="feelingHot">Sentir demasiado calor</Checkbox>
              <Checkbox value="nightmares">Tener pesadillas o malos sueños</Checkbox>
              <Checkbox value="experiencingPain">Sufrir dolores</Checkbox>
              <Checkbox
                value="otherReasonsCheck"
                onChange={(e) => handleSleepProblemsChange('otherReasonsCheck', e.target.checked)}
              >
                Otras razones:
              </Checkbox>
              {formData.sleepProblems.otherReasonsCheck && (
                <Input
                  placeholder="Describa otras razones"
                  onChange={(e) =>
                    handleSleepProblemsChange('otherReasons', e.target.value)
                  }
                />
              )}
            </Col>
          </Row>
        </Checkbox.Group>
      </Form.Item>

      <Form.Item  name ='slepTime6' label="6. Durante el último mes, ¿cómo valoraría en conjunto, la calidad de su sueño?">
        <Radio.Group
          value={formData.sleepQuality}
          onChange={(e) => handleChange('sleepQuality', e.target.value)}
        >
          <Radio value="Muy buena">Muy buena</Radio>
          <Radio value="Bastante buena">Bastante buena</Radio>
          <Radio value="Bastante mala">Bastante mala</Radio>
          <Radio value="Muy mala">Muy mala</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name ='slepTime7' label="7. ¿cuántas veces habrá tomado medicinas (por su cuenta o recetadas por el médico) para dormir?">
        <Radio.Group
          value={formData.medicationFrequency}
          onChange={(e) => handleChange('medicationFrequency', e.target.value)}
        >
          <Radio value="Ninguna vez en el último mes">Ninguna vez en el último mes</Radio>
          <Radio value="Menos de 1 vez /semana">Menos de 1 vez /semana</Radio>
          <Radio value="1 o 2 / semana">1 o 2 / semana</Radio>
          <Radio value="3 o más veces/semana">3 o más veces/semana</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name ='slepTime8' label="8. ¿cuántas veces ha sentido somnolencia mientras conducía, comía o desarrollaba alguna otra actividad?">
        <Radio.Group
          value={formData.daytimeDrowsinessFrequency}
          onChange={(e) => handleChange('daytimeDrowsinessFrequency', e.target.value)}
        >
          <Radio value="Ninguna vez en el último mes">Ninguna vez en el último mes</Radio>
          <Radio value="Menos de 1 vez /semana">Menos de 1 vez /semana</Radio>
          <Radio value="1 o 2 / semana">1 o 2 / semana</Radio>
          <Radio value="3 o más veces/semana">3 o más veces/semana</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name ='slepTime9' label="9. Durante el último mes, ¿ha representado para usted mucho problema el tener ánimos para realizar alguna de las actividades detalladas en la pregunta anterior?">
        <Radio.Group
          value={formData.impactOnActivities}
          onChange={(e) => handleChange('impactOnActivities', e.target.value)}
        >
          <Radio value="Ningún problema">Ningún problema</Radio>
          <Radio value="Sólo un leve problema">Sólo un leve problema</Radio>
          <Radio value="Un problema">Un problema</Radio>
          <Radio value="Un grave problema">Un grave problema</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item name ='slepTime10' label="10. ¿Duerme usted solo o acompañado?">
        <Select
          value={formData.sleepCompanionship}
          onChange={(value) => handleChange('sleepCompanionship', value)}
        >
          <Option value="Solo">Solo</Option>
          <Option value="Con alguien en otra habitación">Con alguien en otra habitación</Option>
          <Option value="En la misma habitación, pero en otra cama">En la misma habitación, pero en otra cama</Option>
          <Option value="En la misma cama">En la misma cama</Option>
        </Select>
      </Form.Item>
    </Form>
  );
});

export default SleepScale;
