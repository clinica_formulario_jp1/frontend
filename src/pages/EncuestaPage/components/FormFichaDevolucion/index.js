import React,{forwardRef, useEffect} from 'react';
import { Button, Form, Input, Radio } from 'antd';

const { TextArea } = Input;

const FichaDevolucionPacienteForm  = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <Form.Item label="Presión Arterial (mmHg):" name="presionArterial">
        <Input />
      </Form.Item>

      <Form.Item label="Frecuencia Cardiaca (Latidos/min):" name="frecuenciaCardiaca">
        <Input />
      </Form.Item>

      <Form.Item label="Saturación de Oxígeno (%):" name="saturacionOxigeno">
        <Input />
      </Form.Item>

      <Form.Item label="Índice de Masa Corporal (IMC):" name="imc">
        <Input />
      </Form.Item>

      <Form.Item label="Cintura Abdominal (cm):" name="cinturaAbdominal">
        <Input />
      </Form.Item>

      <Form.Item label="Cintura de Cuello (cm):" name="cinturaCuello">
        <Input />
      </Form.Item>

      <Form.Item label="Electrocardiograma (ECG):" name="electrocardiograma">
        <Radio.Group>
          <Radio value="Normal">Normal</Radio>
          <Radio value="Anormal">Anormal</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="ECO Doppler Cardiaco:" name="ecoDopplerCardiaco">
        <Radio.Group>
          <Radio value="Normal">Normal</Radio>
          <Radio value="Anormal">Anormal</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="ECO Doppler Arterias Carótidas:" name="ecoDopplerCarotidas">
        <Radio.Group>
          <Radio value="Normal">Normal</Radio>
          <Radio value="Anormal">Anormal</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="ECO Doppler Arterias Femorales:" name="ecoDopplerFemorales">
        <Radio.Group>
          <Radio value="Normal">Normal</Radio>
          <Radio value="Anormal">Anormal</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Observaciones generales:" name="observacionesGenerales">
        <TextArea rows={4} />
      </Form.Item>
    </Form>
  );
});

export default FichaDevolucionPacienteForm;
