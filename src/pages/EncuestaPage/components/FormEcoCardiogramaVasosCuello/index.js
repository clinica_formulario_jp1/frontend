import React,{forwardRef, useEffect} from 'react';
import { Form, Radio, Input, Select, Button } from 'antd';

const { Option } = Select;

const EcocardiogramaVasosCuelloForm = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <Form.Item label="Estudio:" name="estudio">
        <Radio.Group>
          <Radio value="Normal">Estudio Normal</Radio>
          <Radio value="Anormal">Estudio Anormal</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Placas:" name="placas">
        <Radio.Group>
          <Radio value="ObstruccionMenor50">Obstrucción menor del 50%</Radio>
          <Radio value="ObstruccionMayor50">Obstrucción mayor del 50%</Radio>
          <Radio value="Aneurisma">Aneurisma</Radio>
          <Radio value="Tumor">Tumor</Radio>
          <Radio value="Malformacion">Malformación</Radio>
          <Radio value="PlacaAteroesclerotica">Placa ateroesclerótica</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="CAROTIDA DERECHA - Localización:" name="carotidaDerecha">
        <Input />
      </Form.Item>

      <Form.Item label="CAROTIDA DERECHA - Placas:" name="carotidaDerechaPlacas">
        <Select mode="multiple">
          <Option value="TipoI">Tipo I</Option>
          <Option value="TipoII">Tipo II</Option>
          <Option value="TipoIII">Tipo III</Option>
          <Option value="TipoIV">Tipo IV</Option>
          <Option value="TipoV">Tipo V</Option>
        </Select>
      </Form.Item>

      <Form.Item label="CAROTIDA IZQUIERDA - Placas:" name="carotidaIzquierdaPlacas">
        <Select mode="multiple">
          <Option value="TipoI">Tipo I</Option>
          <Option value="TipoII">Tipo II</Option>
          <Option value="TipoIII">Tipo III</Option>
          <Option value="TipoIV">Tipo IV</Option>
          <Option value="TipoV">Tipo V</Option>
        </Select>
      </Form.Item>

      <Form.Item label="CAROTIDA IZQUIERDA - Descripción adicional:" name="carotidaIzquierdaDescripcion">
        <Input />
      </Form.Item>

      <Form.Item label="ECODOPPLER ILEO – FEMORAL - Placas:" name="ecodopplerIleoFemoralPlacas">
        <Select mode="multiple">
          <Option value="Aneurisma">Aneurisma</Option>
          <Option value="Tumor">Tumor</Option>
          <Option value="Malformacion">Malformación</Option>
          <Option value="PlacaAteroesclerotica">Placa ateroesclerótica</Option>
          <Option value="ObstruccionMenor50">Obstrucción menor del 50%</Option>
          <Option value="ObstruccionMayor50">Obstrucción mayor del 50%</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Eje IleoFemoral Derecho:" name="ejeIleoFemoralDerecho">
        <Input />
      </Form.Item>

      <Form.Item label="Eje IleoFemoral Izquierdo:" name="ejeIleoFemoralIzquierdo">
        <Input />
      </Form.Item>

      <Form.Item label="Placas - No calcificada:" name="placasNoCalcificada">
        <Input />
      </Form.Item>

      <Form.Item label="Placas - Calcificada:" name="placasCalcificada">
        <Input />
      </Form.Item>

      <Form.Item label="Descripción adicional - ECODOPPLER ILEO – FEMORAL:" name="ecodopplerIleoFemoralDescripcion">
        <Input />
      </Form.Item>
    </Form>
  );
});

export default EcocardiogramaVasosCuelloForm;
