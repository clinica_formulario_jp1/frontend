import React, { useState, forwardRef, useEffect } from 'react';
import { Form, Input, Button, Table, Space } from 'antd';

const ParametrosAntropometricosForm = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [data, setData] = useState([
    {
      key: '1',
      parameter: 'PRESIÓN ARTERIAL (Brazo Izquierdo)',
      toma1Name: 'presionArterial1',
      toma2Name: 'presionArterial2',
      toma3Name: 'presionArterial3',
      promedioName: 'promedioPresionArterial',
    },
    {
      key: '2',
      parameter: 'TA máxima',
      toma1Name: 'taMaxima',
      toma2Name: 'taMaxima2',
      toma3Name: 'taMaxima3',
      promedioName: 'promedioTaMaxima',
    },
    {
      key: '3',
      parameter: 'TA mínima',
      toma1Name: 'taMinima',
      toma2Name: 'taMinima2',
      toma3Name: 'taMinima3',
      promedioName: 'promedioTaMinima',
    },
    {
      key: '4',
      parameter: 'FC',
      toma1Name: 'fc1',
      toma2Name: 'fc2',
      toma3Name: 'fc3',
      promedioName: 'promedioFc',
    },
  ]);

  const [promedios, setPromedios] = useState({});

  const calcularPromedios = () => {
    const promedioData = data.map((item) => {
      const toma1 = parseFloat(form.getFieldValue(item.toma1Name));
      const toma2 = parseFloat(form.getFieldValue(item.toma2Name));
      const toma3 = parseFloat(form.getFieldValue(item.toma3Name));

      const promedio = ((toma1 + toma2 + toma3) / 3).toFixed(2);

      return {
        ...item,
        promedio,
      };
    });

    setData(promedioData);

    const promedioValues = {};
    promedioData.forEach((item) => {
      promedioValues[item.promedioName] = item.promedio;
    });
    setPromedios(promedioValues);
  };

  const columns = [
    {
      title: 'Parámetro',
      dataIndex: 'parameter',
      key: 'parameter',
    },
    {
      title: '1ª Toma',
      dataIndex: 'toma1',
      key: 'toma1',
      render: (text, record) => (
        <Form.Item name={record.toma1Name}>
          <Input type="number" placeholder="mmHg" />
        </Form.Item>
      ),
    },
    {
      title: '2ª Toma',
      dataIndex: 'toma2',
      key: 'toma2',
      render: (text, record) => (
        <Form.Item name={record.toma2Name}>
          <Input type="number" placeholder="mmHg" />
        </Form.Item>
      ),
    },
    {
      title: '3ª Toma',
      dataIndex: 'toma3',
      key: 'toma3',
      render: (text, record) => (
        <Form.Item name={record.toma3Name}>
          <Input type="number" placeholder="mmHg" />
        </Form.Item>
      ),
    },
    {
      title: 'Promedio',
      dataIndex: 'promedio',
      key: 'promedio',
    },
  ];

  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <h2>Parámetros Antropométricos</h2>
      <Table
        dataSource={data}
        columns={columns}
        pagination={false}
        size="middle"
        bordered
        style={{ marginBottom: '16px' }}
      />
        <Form.Item name="saturacion" style={{ marginBottom: '16px' }} label="Saturación de O2:">
          <Input type="number" placeholder="Saturación de O2" />
        </Form.Item>

        <Form.Item name="peso" style={{ marginBottom: '16px' }}label="PESO (Kg):">
          <Input type="number" placeholder="PESO (Kg)" />
        </Form.Item>

        <Form.Item name="altura" style={{ marginBottom: '16px' }}label="ALTURA (Cm):">
          <Input type="number" placeholder="ALTURA (Cm)" />
        </Form.Item>

        <Form.Item name="cintura" style={{ marginBottom: '16px' }}label="CINTURA (Cm):">
          <Input type="number" placeholder="CINTURA (Cm)" />
        </Form.Item>

        <Form.Item name="abdominal" style={{ marginBottom: '16px' }}label="ABDOMINAL (Cm):">
          <Input type="number" placeholder="ABDOMINAL (Cm)" />
        </Form.Item>

        <Form.Item name="cinturaCuello" style={{ marginBottom: '16px' }}label="CINTURA DE CUELLO (Cm):">
          <Input type="number" placeholder="CINTURA DE CUELLO (Cm)" />
        </Form.Item>

        <Form.Item name="envergadura" style={{ marginBottom: '16px' }}label="ENVERGADURA (Cm):">
          <Input type="number" placeholder="ENVERGADURA (Cm)" />
        </Form.Item>

            <Form.Item
              label="CIRCUNFERENCIA BRAQUIAL Derecha (Cm):"
              name="circunferenciaBraquialDerecha"
              style={{ marginBottom: '16px' }}
            >
              <Input type="number" placeholder="Derecha" />
            </Form.Item>

            <Form.Item
              label="CIRCUNFERENCIA BRAQUIAL Izquierda (Cm):"
              name="circunferenciaBraquialIzquierda"
              style={{ marginBottom: '16px' }}
            >
              <Input type="number" placeholder="Izquierda" />
            </Form.Item>

            <Form.Item
             label="CIRCUNFERENCIA PANTORRILLA Derecha (Cm):"
              name="circunferenciaPantorillaDerecha"
              style={{ marginBottom: '16px' }}
            >
              <Input type="number" placeholder="Derecha" required />
            </Form.Item>

            <Form.Item
             label="CIRCUNFERENCIA PANTORRILLA Izquierda (Cm):"
              name="circunferenciaPantorillaIzquierda"
              style={{ marginBottom: '16px' }}
            >
              <Input type="number" placeholder="Izquierda" required />
            </Form.Item>
      <Space style={{ padding: '16px' }}>
        <Button type="primary" onClick={calcularPromedios}>
          Calcular Promedios
        </Button>
      </Space>
    </Form>
  );
});

export default ParametrosAntropometricosForm;
