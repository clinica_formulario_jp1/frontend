import React ,{forwardRef, useEffect}from 'react';
import { Form, Input, Radio, Select, Button } from 'antd';

const { Option } = Select;

const EcocardiogramaDopplerColorForm = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >

      <Form.Item label="Nombre y Apellido:" name="nombreYApellido">
        <Input />
      </Form.Item>

      <Form.Item label="Edad:" name="edad">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Sexo:" name="sexo">
        <Radio.Group>
          <Radio value="M">M</Radio>
          <Radio value="F">F</Radio>
        </Radio.Group>
      </Form.Item>

      <h3>Dimensiones (en mm)</h3>

      <Form.Item label="DDVI:" name="ddvi">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="DSVI:" name="dsvi">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="FAC:" name="fac">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="SIV:" name="siv">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="PP:" name="pp">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="TSVI:" name="tsvi">
        <Input type="number" />
      </Form.Item>

      <h3>Doppler (cm/seg)</h3>

      <Form.Item label="TaccPulm:" name="taccPulm">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Onda E:" name="ondaE">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Onda A:" name="ondaA">
        <Input type="number" />
      </Form.Item>

      <h3>Doppler Tisular (cm/seg)</h3>

      <Form.Item label="PLVI onda S:" name="plviOndaS">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Insuficiencia:" name="insuficiencia">
        <Select>
          <Option value="Moderada">Moderada</Option>
          <Option value="Severa">Severa</Option>
        </Select>
      </Form.Item>

      <Form.Item label="SI/NO Valvulas Mitral:" name="valvulasMitral">
        <Select>
          <Option value="SI">SI</Option>
          <Option value="NO">NO</Option>
        </Select>
      </Form.Item>

      <Form.Item label="SI/NO Valvulas Aortica:" name="valvulasAortica">
        <Select>
          <Option value="SI">SI</Option>
          <Option value="NO">NO</Option>
        </Select>
      </Form.Item>

      <Form.Item label="SI/NO Valvulas Tricuspidea:" name="valvulasTricuspidea">
        <Select>
          <Option value="SI">SI</Option>
          <Option value="NO">NO</Option>
        </Select>
      </Form.Item>

      <Form.Item label="SI/NO Valvulas Pulmonar:" name="valvulasPulmonar">
        <Select>
          <Option value="SI">SI</Option>
          <Option value="NO">NO</Option>
        </Select>
      </Form.Item>

      <Form.Item label="onda e':" name="ondaE2">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="onda a':" name="ondaA2">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Estenosis:" name="estenosis">
        <Select>
          <Option value="Moderada">Moderada</Option>
          <Option value="Severa">Severa</Option>
        </Select>
      </Form.Item>

      <Form.Item label="SI/NO Esclerosis Aortica:" name="esclerosisAortica">
        <Select>
          <Option value="SI">SI</Option>
          <Option value="NO">NO</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Auricula Izquierda Area Biplano (cm2):" name="auriculaIzquierdaArea">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="SI/NO Calcificacion del anillo mitral:" name="calcificacionMitral">
        <Select>
          <Option value="SI">SI</Option>
          <Option value="NO">NO</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Auricula Derecha Area (cm2):" name="auriculaDerechaArea">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Ventriculo izquierdo. Fraccion de eyeccion (visual):" name="fraccionEyeccion">
        <Select>
          <Option value="Normal (>52% h, >54% m)">Normal (mayor 52% h, mayor54% m)</Option>
          <Option value="Leve (51-41% h, 53-41% m)">Leve (51-41% h, 53-41% m)</Option>
          <Option value="Moderada (30-40% ambos)">Moderada (30-40% ambos)</Option>
          <Option value="Severa (<30% ambos)">Severa (menor 30% ambos)</Option>
        </Select>
      </Form.Item>

      <Form.Item label="SI/NO Segmentarismos:" name="segmentarismos">
        <Select>
          <Option value="SI">SI</Option>
          <Option value="NO">NO</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Función DDVD (≤42 VD/VI):" name="funcionDDVD">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Vena Cava Inferior:" name="venaCavaInferior">
        <Select>
          <Option value="<1<">{'<1<'}</Option>
          <Option value="Si">Si</Option>
          <Option value="Moderado">Moderado</Option>
          <Option value="Severo">Severo</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Derrame Otro:" name="derrameOtro">
        <Input />
      </Form.Item>

      <Form.Item label="TAPSE (mm):" name="tapse">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Onda S (cm/seg):" name="ondaS">
        <Input type="number" />
      </Form.Item>

      <Form.Item label="Colapso >50%:" name="colapsoMayor50">
        <Select>
          <Option value="Si">Si</Option>
          <Option value="No">No</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Colapso <50%:" name="colapsoMenor50">
        <Select>
          <Option value="Si">Si</Option>
          <Option value="No">No</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Dimensiones Dimensiones 21 mm o <>21 mm:" name="dimensiones21mm">
        <Input />
      </Form.Item>

      <Form.Item label="Pericardio:" name="pericardio">
        <Select>
          <Option value="Normal">Normal</Option>
        </Select>
      </Form.Item>

      <Form.Item label="Hallazgos adicionales:" name="hallazgosAdicionales">
        <Input />
      </Form.Item>
    </Form>
  );
});

export default EcocardiogramaDopplerColorForm;
