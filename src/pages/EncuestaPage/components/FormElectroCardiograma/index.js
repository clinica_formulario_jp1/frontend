import React ,{forwardRef, useEffect} from 'react';
import { Form, Input, Radio, Button } from 'antd';

const ElectrocardiogramForm = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <Form.Item
        label="1) RITMO:"
        name="ritmo"
        rules={[{ required: true, message: 'Por favor, selecciona el ritmo cardíaco' }]}
      >
        <Radio.Group>
          <Radio value="sinusal">Sinusal</Radio>
          <Radio value="no_sinusal">No Sinusal</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        label="2) FRECUENCIA CARDIACA:"
        name="frecuencia_cardiaca"
        rules={[{ required: true, message: 'Por favor, ingresa la frecuencia cardíaca' }]}
      >
        <Input addonAfter="lpm" />
      </Form.Item>
      <Form.Item label="3) EJE ELECTRICO DEL QRS: Poner Valor:" name="eje_qrs">
        <Input placeholder="Valor en grados" />
      </Form.Item>
      <Form.Item label="4) ONDA P:" name="onda_p">
        <Radio.Group>
          <Radio value="presente">Presente</Radio>
          <Radio value="no_presente">No presente</Radio>
        </Radio.Group>
        <Input
          placeholder="DURACION en mseg"
          name="onda_p_duracion"
          style={{ marginTop: '8px' }}
        />
      </Form.Item>
      <Form.Item label="5) INTERVALO PR:" name="intervalo_pr">
        <Input placeholder="DURACION en mseg" />
      </Form.Item>
      <Form.Item label="6) QRS:" name="qrs">
        <Input placeholder="DURACION en mseg" />
        <Input placeholder="AMPLITUD en mm" style={{ marginTop: '8px' }} />
      </Form.Item>
      <Form.Item label="7) SEGMENTO ST:" name="segmento_st">
        <Radio.Group>
          <Radio value="normal">Normal</Radio>
          <Radio value="anormal">Anormal</Radio>
        </Radio.Group>
        <Input placeholder="AMPLITUD en mm" name="segmento_st_amplitud" style={{ marginTop: '8px' }} />
        <Input placeholder="Otro" name="segmento_st_otro1" style={{ marginTop: '8px' }} />
        <Input placeholder="Otro" name="segmento_st_otro2" style={{ marginTop: '8px' }} />
      </Form.Item>
      <Form.Item label="8) INTERVALO QT:" name="intervalo_qt">
        <Input placeholder="DURACION" />
        <Input placeholder="En caso de ser Anormal especificar" name="intervalo_qt_anormal" style={{ marginTop: '8px' }} />
      </Form.Item>
      <Form.Item label="CONCLUSIONES:" name="conclusiones">
        <Radio.Group>
          <Radio value="normal">NORMAL</Radio>
          <Radio value="supradesnivel">Supradesnivel</Radio>
          <Radio value="infradesnivel">Infradesnivel</Radio>
          <Radio value="anormal_largo">Anormal Largo</Radio>
          <Radio value="anormal_corto">Anormal Corto</Radio>
        </Radio.Group>
        <Input placeholder="En caso de ser Anormal especificar" name="conclusiones_anormal" style={{ marginTop: '8px' }} />
      </Form.Item>
    </Form>
  );
});

export default ElectrocardiogramForm;
