import React, { useRef , forwardRef, useEffect} from 'react';
import { Form, Input, Select, Checkbox, Row, Col, Button } from 'antd';
import { useParams } from "react-router-dom";
const { Option } = Select;

const FormEncuestaCardioVascularDirigida = forwardRef(({ onFormSubmit, formData }, ref) => {
   const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <div>
      <Form
        form={form}
        onFinish={onFinish}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={formData}
        ref={ref}
      >
         {/* Información Personal */}
        <Form.Item label="Nombre y Apellido" name="nombreApellido" rules={[{ required: true, message: 'Campo requerido' }]}>
          <Input />
        </Form.Item>
        <Form.Item label="Fecha de Nacimiento" name="fechaNacimiento" rules={[{ required: true, message: 'Campo requerido' }]}>
          <Input type='date'/>
        </Form.Item>
        <Form.Item label="Sexo" name="sexo">
          <Input />
        </Form.Item>
        <Form.Item label="Edad" name="edad" rules={[{ required: true, message: 'Campo requerido' }]}>
          <Input />
        </Form.Item>
        <Form.Item label="DNI" name="dni" rules={[{ required: true, message: 'Campo requerido' }]}>
          <Input />
        </Form.Item>
        {/* Contacto */}
        <Form.Item label="Contacto Tel F/cel" name="contacto" >
          <Input type="tel" />
        </Form.Item>

        {/* Dirección */}
        <Form.Item label="Dirección" name="direccion">
          <Input />
        </Form.Item>

        {/* Nivel de Estudios */}
        <Form.Item label="Nivel de Estudios" name="nivelEstudios">
          <Select>
            <Option value="Analfabeto">Analfabeto</Option>
            <Option value="Primario">Primario</Option>
            <Option value="Secundario">Secundario</Option>
            <Option value="Terciario">Terciario</Option>
            <Option value="Universitario">Universitario</Option>
          </Select>
        </Form.Item>

        {/* Cobertura Médica */}
        <Form.Item label="Cobertura Médica" name="coberturaMedica">
          <Checkbox.Group>
            <Row>
              <Col span={6}><Checkbox value="Obra Social">Obra Social</Checkbox></Col>
              <Col span={6}><Checkbox value="Prepago">Prepago</Checkbox></Col>
              <Col span={6}><Checkbox value="PAMI">PAMI</Checkbox></Col>
              <Col span={6}><Checkbox value="Sin Cobertura">Sin Cobertura</Checkbox></Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>

        {/* Ocupación */}
        <Form.Item label="Ocupación" name="ocupacion">
          <Checkbox.Group>
            <Row>
              <Col span={6}><Checkbox value="Activo">Activo</Checkbox></Col>
              <Col span={6}><Checkbox value="Jubilado">Jubilado</Checkbox></Col>
              <Col span={6}><Checkbox value="Desocupado">Desocupado</Checkbox></Col>
              <Col span={6}><Checkbox value="Ama de Casa">Ama de Casa</Checkbox></Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>

        {/* Encuesta Cardiovascular */}
        <Form.Item label="Encuesta Cardiovascular (Cuales son)" name="encuestaCardiovascular">
          <Input.TextArea className="observaciones" />
        </Form.Item>

        {/* Medicación */}
        <Form.Item label="Medicación" name="medicacion">
          <Input />
        </Form.Item>
        <Form.Item label="N° pastillas" name="pastillas">
          <Input type="number" />
        </Form.Item>
        <Form.Item label="Toma/día" name="tomaDia">
          <Input />
        </Form.Item>

        {/* Embarazo */}
        <Form.Item label="Cuanto pesaba al nacer" name="pesoNacer">
          <Input />
        </Form.Item>
        <Form.Item label="A las cuantas semanas nació" name="semanasNacimiento">
          <Input />
        </Form.Item>
        <Form.Item label="Tuvo HTA en el embarazo?" name="htaEmbarazo">
          <Select>
            <Option value="SI">SI</Option>
            <Option value="NO">NO</Option>
            <Option value="NS">NS</Option>
          </Select>
        </Form.Item>

        {/* Antecedentes Familiares */}
        <Form.Item label="Antecedentes Familiares">
          <Form.Item label="Padre: ¿está vivo?" name="padreVivo" style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}>
            <Select>
              <Option value="SI">SI</Option>
              <Option value="NO">NO</Option>
              <Option value="NS">NS</Option>
            </Select>
          </Form.Item>
          <Form.Item label="A qué edad murió?" name="edadPadre" style={{ display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px' }}>
            <Input />
          </Form.Item>
          <Form.Item label="Madre: ¿está viva?" name="madreViva" style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}>
            <Select>
              <Option value="SI">SI</Option>
              <Option value="NO">NO</Option>
              <Option value="NS">NS</Option>
            </Select>
          </Form.Item>
          <Form.Item label="A qué edad murió?" name="edadMadre" style={{ display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px' }}>
            <Input />
          </Form.Item>
        </Form.Item>

        {/* Alcohol y Actividad Física */}
        <Form.Item label="Alcohol y Actividad Física">
          <Form.Item label="¿Consume alcohol?" name="consumoAlcohol" style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}>
            <Select>
              <Option value="SI">SI</Option>
              <Option value="NO">NO</Option>
              <Option value="NS">NS</Option>
            </Select>
          </Form.Item>
          <Form.Item label="¿Cuánto toma por día?" name="cantidadAlcohol" style={{ display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px' }}>
            <Input />
          </Form.Item>
          <Form.Item label="Hace 30 min/día de actividad física" name="actividadFisica" style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}>
            <Select>
              <Option value="SI">SI</Option>
              <Option value="NO">NO</Option>
              <Option value="NS">NS</Option>
            </Select>
          </Form.Item>
          <Form.Item label="Cuántas veces por semana?" name="frecuenciaActividad" style={{ display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px' }}>
            <Input />
          </Form.Item>
        </Form.Item>

       

        {/* Observaciones */}
        <Form.Item label="Observaciones" name="observaciones">
          <Input.TextArea className="observaciones" />
        </Form.Item>

        {/* Teléfono Celular */}
        <Form.Item label="Teléfono Celular" name="telefonoCelular">
          <Input type="tel" />
        </Form.Item>
        <Form.Item label="¿Tiene celular?" name="usoCelular">
          <Select>
            <Option value="SI">SI</Option>
            <Option value="NO">NO</Option>
            <Option value="NS">NS</Option>
          </Select>
        </Form.Item>
        <Form.Item label="¿Usa para llamadas?" name="usoLlamadas">
          <Select>
            <Option value="SI">SI</Option>
            <Option value="NO">NO</Option>
            <Option value="NS">NS</Option>
          </Select>
        </Form.Item>
        <Form.Item label="¿Usa para mensajes?" name="usoMensajes">
          <Select>
            <Option value="SI">SI</Option>
            <Option value="NO">NO</Option>
            <Option value="NS">NS</Option>
          </Select>
        </Form.Item>
        <Form.Item label="¿Usa WhatsApp?" name="usoWhatsApp">
          <Select>
            <Option value="SI">SI</Option>
            <Option value="NO">NO</Option>
            <Option value="NS">NS</Option>
          </Select>
        </Form.Item>
        <Form.Item label="Horas al día usando el celular" name="horasCelular">
          <Select>
            <Option value="<11">Menos de 11 horas</Option>
            <Option value="11-3">11-3 horas</Option>
            <Option value="3-6">3-6 horas</Option>
            <Option value=">6">Más de 6 horas</Option>
          </Select>
        </Form.Item>
      </Form>
    </div>
  );
});

export default FormEncuestaCardioVascularDirigida;
