import React, { useState ,forwardRef, useEffect} from 'react';
import { Form, Radio, Button } from 'antd';

const RosenbergSelfEsteemScale = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [answers, setAnswers] = useState({
    q1: null,
    q2: null,
    q3: null,
    q4: null,
    q5: null,
    q6: null,
    q7: null,
    q8: null,
    q9: null,
    q10: null,
  });

  const onFinish = (values) => {
    onFormSubmit(values);
  };

  const handleAnswerChange = (question, value) => {
    setAnswers({ ...answers, [question]: value });
  };
  const [form] = Form.useForm();
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <h2>Escala de Autoestima (Rosenberg)</h2>
      <table>
        <thead>
          <tr>
            <th>Pregunta</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <Form.Item
                label="1. Siento que soy una persona digna de aprecio/valiosa, al menos en igual medida que los demás."
                name="q1"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q1"
                  value={answers.q1}
                  onChange={(e) => handleAnswerChange('q1', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="2. Creo que tengo varias cualidades buenas."
                name="q2"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q2"
                  value={answers.q2}
                  onChange={(e) => handleAnswerChange('q2', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="3. Me inclino a pensar que, en conjunto, soy un/a fracasado/a."
                name="q3"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q3"
                  value={answers.q3}
                  onChange={(e) => handleAnswerChange('q3', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="4. Puedo hacer las cosas tan bien como la mayoría de la gente."
                name="q4"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q4"
                  value={answers.q4}
                  onChange={(e) => handleAnswerChange('q4', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="5. Creo que no tengo muchos motivos para sentirme orgulloso/a de mí."
                name="q5"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q5"
                  value={answers.q5}
                  onChange={(e) => handleAnswerChange('q5', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="6. Tengo una actitud positiva hacia mí mismo/a."
                name="q6"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q6"
                  value={answers.q6}
                  onChange={(e) => handleAnswerChange('q6', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="7. En general, estoy satisfecho/a conmigo mismo/a."
                name="q7"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q7"
                  value={answers.q7}
                  onChange={(e) => handleAnswerChange('q7', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="8. Debería valorarme más a mí mismo/a."
                name="q8"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q8"
                  value={answers.q8}
                  onChange={(e) => handleAnswerChange('q8', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="9. A veces me siento verdaderamente inútil."
                name="q9"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q9"
                  value={answers.q9}
                  onChange={(e) => handleAnswerChange('q9', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>
              <Form.Item
                label="10. A veces pienso que no sirvo para nada."
                name="q10"
                rules={[
                  {
                    required: true,
                    message: 'Por favor, seleccione una respuesta.',
                  },
                ]}
              >
                <Radio.Group
                  name="q10"
                  value={answers.q10}
                  onChange={(e) => handleAnswerChange('q10', e.target.value)}
                >
                  <Radio value="4">Totalmente de Acuerdo</Radio>
                  <Radio value="3">De Acuerdo</Radio>
                  <Radio value="2">En Desacuerdo</Radio>
                  <Radio value="1">Totalmente en Desacuerdo</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
        </tbody>
      </table>
    </Form>
  );
});

export default RosenbergSelfEsteemScale;
