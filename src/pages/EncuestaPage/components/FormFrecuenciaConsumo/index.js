import React, { useState,forwardRef,useImperativeHandle, useEffect } from 'react';
import { Form, Checkbox, Button, Table } from 'antd';

const alimentos = [
  'Lácteos Enteros',
  'Lácteos Semi/Desnatados',
  'Huevos',
  'Carnes Magras',
  'Carnes Grasas',
  'Pescado Blanco',
  'Pescado Azul',
  'Verduras',
  'Frutas',
  'Frutos Secos',
  'Legumbres',
  'Aceite de Oliva',
  'Otras Grasas',
  'Cereales Refinados',
  'Cereales Integrales',
  'Repostería Industrial',
  'Azúcares',
  'Alcohol',
  'Agua',
];

const MyFoodFrequencyForm = forwardRef(({ onFormSubmit, onConsumoChange, formData }, ref) => {
  const [consumo, setConsumo] = useState({});
  const [form] = Form.useForm();
  const handleInputChange = (alimento, tipo, valores) => {
    setConsumo({
      ...consumo,
      [alimento]: {
        ...consumo[alimento],
        [tipo]: valores,
      },
    });
  };

  const columns = [
    {
      title: 'Grupo de Alimentos',
      dataIndex: 'alimento',
      key: 'alimento',
    },
    {
      title: 'Nunca',
      dataIndex: 'nunca',
      key: 'nunca',
    },
    {
      title: 'Al Mes',
      dataIndex: 'mes',
      key: 'mes',
    },
    {
      title: 'A la Semana',
      dataIndex: 'semana',
      key: 'semana',
    },
    {
      title: 'Al Día',
      dataIndex: 'dia',
      key: 'dia',
    },
  ];

  const dataSource = alimentos.map((alimento) => ({
    key: alimento,
    alimento,
    nunca: (
      <Checkbox
        value="Nunca"
        checked={consumo[alimento]?.nunca}
        onChange={(e) =>
          handleInputChange(alimento, 'nunca', e.target.checked)
        }
      />
    ),
    mes: (
      <Checkbox.Group
        options={['1', '2', '3']}
        value={consumo[alimento]?.mes}
        onChange={(valores) =>
          handleInputChange(alimento, 'mes', valores)
        }
      />
    ),
    semana: (
      <Checkbox.Group
        options={['1', '2', '3', '4', '5', '6']}
        value={consumo[alimento]?.semana}
        onChange={(valores) =>
          handleInputChange(alimento, 'semana', valores)
        }
      />
    ),
    dia: (
      <Checkbox.Group
        options={['1', '2', '3', '4', '5', '>=6']}
        value={consumo[alimento]?.dia}
        onChange={(valores) =>
          handleInputChange(alimento, 'dia', valores)
        }
      />
    ),
  }));

  const getFieldsValue = () => {
    return consumo;
  };
  const handleFormSubmit = async () => {
    try {
      const values = await form.validateFields(); // Valida todos los campos del formulario
    } catch (errorInfo) {
      console.error('Error de validación:', errorInfo);
    }
  };

   useImperativeHandle(ref, () => ({
    getFieldsValue,
    validateFields: handleFormSubmit,
  }));

  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <Form.Item name='MyFoodFrequencyForm' label="CUESTIONARIO DE FRECUENCIA DE CONSUMO DE ALIMENTOS">
        <p>Consumo medio durante el año pasado</p>
      </Form.Item>
      <Table columns={columns} dataSource={dataSource} pagination={false} />
    </Form>
  );
});

export default MyFoodFrequencyForm;
