import React ,{forwardRef, useEffect} from 'react';
import { Form, Input, Radio,Button } from 'antd';

const { TextArea } = Input;

const EjercicioForm = forwardRef(({ onFormSubmit, formData }, ref)  => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <Form.Item label="Pre esfuerzo (lat/min):" name="preEsfuerzo">
        <Input />
      </Form.Item>

      <Form.Item label="Intra esfuerzo (lat/min):" name="intraEsfuerzo">
        <Input />
      </Form.Item>

      <Form.Item label="Post esfuerzo (lat/min):" name="postEsfuerzo">
        <Input />
      </Form.Item>

      <Form.Item label="Número de sentadillas:" name="numSentadillas">
        <Input />
      </Form.Item>

      <Form.Item label="Normal / Anormal:" name="normalAnormal">
        <Radio.Group>
          <Radio value="Normal">Normal</Radio>
          <Radio value="Anormal">Anormal</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Valor 1 (Kg):" name="valor1">
        <Input />
      </Form.Item>

      <Form.Item label="Valor 2 (Kg):" name="valor2">
        <Input />
      </Form.Item>

      <Form.Item label="Valor 3 (Kg):" name="valor3">
        <Input />
      </Form.Item>

      <Form.Item label="Normal / Anormal (Dinamómetro en mano dominante):" name="normalAnormalDinamometro">
        <Radio.Group>
          <Radio value="Normal">Normal</Radio>
          <Radio value="Anormal">Anormal</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Comentarios adicionales:" name="comentarios">
        <TextArea rows={4} />
      </Form.Item>
    </Form>
  );
})

export default EjercicioForm;
