import React, { useState ,forwardRef, useEffect} from 'react';
import { Form, Radio, Input, Button } from 'antd';

const EdmontonScaleForm = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [points, setPoints] = useState({
    cognitivo: 0,
    salud_1: 0,
    salud_2: 0,
    indepFunc: 0,
    sopSocial: 0,
    usoMedicam: 0,
    usoMedicam2: 0,
    nutricion: 0,
    animo: 0,
    continencia: 0,
    rendimientoFuncional: 0,
  });


  const handlePointChange = (fieldName, value) => {
    setPoints((prevPoints) => ({
      ...prevPoints,
      [fieldName]: value,
    }));
  };

  const calculateTotalPoints = () => {
    return Object.values(points).reduce((total, value) => total + value, 0);
  };
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <h2>Escala de Edmonton</h2>
      <table>
        <thead>
          <tr>
            <th>Dominio de Fragilidad</th>
            <th>Item</th>
            <th>Puntos&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;2</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Cognitivo</td>
            <td>
              Por favor, imagine que este círculo pre-dibujado es un reloj. Marque los números en las posiciones correctas y luego
              coloque las manecillas (horaria y minutera) para indicar la hora "Las once con diez minutos".
            </td>
            <td>
              <Form.Item name="cognitivo" initialValue={0}>
                <Radio.Group
                  name="cognitivo"
                  value={points.cognitivo}
                  onChange={(e) => handlePointChange('cognitivo', parseInt(e.target.value))}
                >
                  <td>
                  <Radio value={0}>Sin errores</Radio>
                  </td>
                  <td>
                  <Radio value={1}>Errores mínimos de espacio</Radio>
                  </td>
                  <td>
                  <Radio value={2}>Otros errores</Radio>
                  </td>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Estado de Salud General</td>
            <td>
              En el último año, ¿cuántas veces ha estado hospitalizado?
            </td>
            <td>
              <Form.Item name="salud_1" initialValue={0}>
                <Radio.Group
                  name="salud_1"
                  value={points.salud_1}
                  onChange={(e) => handlePointChange('salud_1', parseInt(e.target.value))}
                >
                  <Radio value={0}>0</Radio>
                  <Radio value={1}>1-2</Radio>
                  <Radio value={2}>>= 3</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              En general, ¿cómo describiría su salud?
            </td>
            <td>
              <Form.Item name="salud_2" initialValue={0}>
                <Radio.Group
                  name="salud_2"
                  value={points.salud_2}
                  onChange={(e) => handlePointChange('salud_2', parseInt(e.target.value))}
                >
                  <Radio value={0}>Excelente</Radio>
                  <Radio value={1}>Razonable</Radio>
                  <Radio value={2}>Mala</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Independencia Funcional</td>
            <td>
              ¿Con cuántas de las siguientes actividades necesita ayuda? (preparar la comida, compras, transporte, comunicación
              telefónica, cuidado del hogar, lavado de ropa, manejo de dinero, tomar medicamentos)
            </td>
            <td>
              <Form.Item name="indepFunc" initialValue={0}>
                <Radio.Group
                  name="indepFunc"
                  value={points.indepFunc}
                  onChange={(e) => handlePointChange('indepFunc', parseInt(e.target.value))}
                >
                  <Radio value={0}>0-1</Radio>
                  <Radio value={1}>2-4</Radio>
                  <Radio value={2}>>= 4</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Soporte Social</td>
            <td>
              ¿Cuando usted necesita ayuda, puede contar con alguien que esté dispuesto y disponible para atender sus necesidades o problemas?
            </td>
            <td>
              <Form.Item name="sopSocial" initialValue={0}>
                <Radio.Group
                  name="sopSocial"
                  value={points.sopSocial}
                  onChange={(e) => handlePointChange('sopSocial', parseInt(e.target.value))}
                >
                  <Radio value={0}>Siempre</Radio>
                  <Radio value={1}>A Veces</Radio>
                  <Radio value={2}>Nunca</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Uso de Medicamentos</td>
            <td>
              ¿Usa 5 o más medicamentos en el día a día?
            </td>
            <td>
              <Form.Item name="usoMedicam" initialValue={0}>
                <Radio.Group
                  name="usoMedicam"
                  value={points.usoMedicam}
                  onChange={(e) => handlePointChange('usoMedicam', parseInt(e.target.value))}
                >
                  <Radio value={0}>No</Radio>
                  <Radio value={1}>Sí</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              En ocasiones, ¿se le olvida tomarse los medicamentos?
            </td>
            <td>
              <Form.Item name="usoMedicam2" initialValue={0}>
                <Radio.Group
                  name="usoMedicam2"
                  value={points.usoMedicam2}
                  onChange={(e) => handlePointChange('usoMedicam2', parseInt(e.target.value))}
                >
                  <Radio value={0}>No</Radio>
                  <Radio value={1}>Sí</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Nutrición</td>
            <td>
              Recientemente, ¿Ha perdido peso como para que su ropa le quede suelta?
            </td>
            <td>
              <Form.Item name="nutricion" initialValue={0}>
                <Radio.Group
                  name="nutricion"
                  value={points.nutricion}
                  onChange={(e) => handlePointChange('nutricion', parseInt(e.target.value))}
                >
                  <Radio value={0}>No</Radio>
                  <Radio value={1}>Sí</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Animo</td>
            <td>
              ¿Se siente con frecuencia triste o deprimido?
            </td>
            <td>
              <Form.Item name="animo" initialValue={0}>
                <Radio.Group
                  name="animo"
                  value={points.animo}
                  onChange={(e) => handlePointChange('animo', parseInt(e.target.value))}
                >
                  <Radio value={0}>No</Radio>
                  <Radio value={1}>Sí</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Continencia</td>
            <td>
              ¿Tiene algún problema con el control para orinar, es decir, puede contener la orina si así lo desea?
            </td>
            <td>
              <Form.Item name="continencia" initialValue={0}>
                <Radio.Group
                  name="continencia"
                  value={points.continencia}
                  onChange={(e) => handlePointChange('continencia', parseInt(e.target.value))}
                >
                  <Radio value={0}>No</Radio>
                  <Radio value={1}>Sí</Radio>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          <tr>
            <td>Rendimiento Funcional</td>
            <td>
              Me gustaría que se sentara en esta silla con su espalda y brazos relajados.
              Luego, cuando yo diga "YA", por favor, pártese y camine a un ritmo cómodo y seguro a la marca del suelo (aproximadamente a 3 metros de distancia).
              Regrese a su silla y siéntese.
            </td>
            <td>
              <Form.Item name="rendimientoFuncional" initialValue={0}>
                <Radio.Group
                  name="rendimientoFuncional"
                  value={points.rendimientoFuncional}
                  onChange={(e) => handlePointChange('rendimientoFuncional', parseInt(e.target.value))}
                >
                  <td>
                  <Radio value={0}>0-10 segundos</Radio>
                  </td>
                  <td>
                  <Radio value={1}>11-20 segundos</Radio>
                  </td>
                  <td>
                  <Radio value={2}>Uno de: &gt; 20s, paciente no quiere o requiere asistencia</Radio>
                  </td>
                </Radio.Group>
              </Form.Item>
            </td>
          </tr>
          {/* Agrega más filas para otros dominios y elementos */}
          <tr>
            <td>Totales</td>
            <td>
              Puntaje final es la suma de las columnas totales
            </td>
            <td colSpan="3">
              <Input type="number" name="totales" value={calculateTotalPoints()} min="0" readOnly />
            </td>
          </tr>
        </tbody>
      </table>
    </Form>
  );
});

export default EdmontonScaleForm;
