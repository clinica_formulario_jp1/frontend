import React ,{forwardRef, useEffect} from 'react';
import { Form, Radio, Button, Select } from 'antd';

const { Option } = Select;

const MyHealthQuestionnaire = forwardRef(({ onFormSubmit, formData }, ref) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    onFormSubmit(values);
  };
  useEffect(() => {
      if (formData !== null) {
        setFormData(formData);
      }
    }, [formData]);
    const setFormData = (data) => {
      form.setFieldsValue(data);
    };

  return (
    <Form
      form={form}
      onFinish={onFinish}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={formData}
      ref={ref}
    >
      <Form.Item
        name="question1"
        label="1. En general, usted diría que su salud es:"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Excelente">Excelente</Radio>
          <Radio value="Muy buena">Muy buena</Radio>
          <Radio value="Buena">Buena</Radio>
          <Radio value="Regular">Regular</Radio>
          <Radio value="Mala">Mala</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question2"
        label="2. Su salud actual, ¿le limita para hacer esfuerzos moderados, como mover una mesa, pasar la aspiradora, jugar a los bolos o caminar más de una hora?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Sí, me limita mucho">Sí, me limita mucho</Radio>
          <Radio value="Sí, me limita un poco">Sí, me limita un poco</Radio>
          <Radio value="No, no me limita nada">No, no me limita nada</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question3"
        label="3. Su salud actual, ¿le limita para subir varios pisos por la escalera?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Sí, me limita mucho">Sí, me limita mucho</Radio>
          <Radio value="Sí, me limita un poco">Sí, me limita un poco</Radio>
          <Radio value="No, no me limita nada">No, no me limita nada</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question4"
        label="4. Durante las 4 últimas semanas, ¿hizo menos de lo que hubiera querido hacer, a causa de su salud física?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Sí">Sí</Radio>
          <Radio value="No">No</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question5"
        label="5. Durante las 4 últimas semanas, ¿tuvo que dejar de hacer algunas tareas en su trabajo o en sus actividades cotidianas, a causa de su salud física?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Sí">Sí</Radio>
          <Radio value="No">No</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question6"
        label="6. Durante las 4 últimas semanas, ¿hizo menos de lo que hubiera querido hacer, a causa de algún problema emocional (como estar triste, deprimido, o nervioso)?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Sí">Sí</Radio>
          <Radio value="No">No</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question7"
        label="7. Durante las 4 últimas semanas, ¿no hizo su trabajo o sus actividades cotidianas tan cuidadosamente como de costumbre, a causa de algún problema emocional (como estar triste, deprimido, o nervioso)?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Sí">Sí</Radio>
          <Radio value="No">No</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question8"
        label="8. Durante las 4 últimas semanas, ¿hasta qué punto el dolor le ha dificultado su trabajo habitual (incluido el trabajo fuera de casa y las tareas domésticas)?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Radio.Group>
          <Radio value="Nada">Nada</Radio>
          <Radio value="Un poco">Un poco</Radio>
          <Radio value="Regular">Regular</Radio>
          <Radio value="Bastante">Bastante</Radio>
          <Radio value="Mucho">Mucho</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name="question9"
        label="9. Durante las 4 últimas semanas, ¿cuánto tiempo se sintió calmado y tranquilo?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Select>
          <Option value="Siempre">Siempre</Option>
          <Option value="Casi siempre">Casi siempre</Option>
          <Option value="Algunas veces">Algunas veces</Option>
          <Option value="Muchas veces">Muchas veces</Option>
          <Option value="Sólo alguna vez">Sólo alguna vez</Option>
          <Option value="Nunca">Nunca</Option>
        </Select>
      </Form.Item>

      <Form.Item
        name="question10"
        label="10. Durante las 4 últimas semanas, ¿cuánto tiempo tuvo mucha energía?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Select>
          <Option value="Siempre">Siempre</Option>
          <Option value="Casi siempre">Casi siempre</Option>
          <Option value="Algunas veces">Algunas veces</Option>
          <Option value="Muchas veces">Muchas veces</Option>
          <Option value="Sólo alguna vez">Sólo alguna vez</Option>
          <Option value="Nunca">Nunca</Option>
        </Select>
      </Form.Item>

      <Form.Item
        name="question11"
        label="11. Durante las 4 últimas semanas, ¿cuánto tiempo se sintió desanimado y triste?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Select>
          <Option value="Siempre">Siempre</Option>
          <Option value="Casi siempre">Casi siempre</Option>
          <Option value="Algunas veces">Algunas veces</Option>
          <Option value="Muchas veces">Muchas veces</Option>
          <Option value="Sólo alguna vez">Sólo alguna vez</Option>
          <Option value="Nunca">Nunca</Option>
        </Select>
      </Form.Item>

      <Form.Item
        name="question12"
        label="12. Durante las 4 últimas semanas, ¿con qué frecuencia la salud física o los problemas emocionales le han dificultado sus actividades sociales (como visitar a los amigos o familiares)?"
        rules={[{ required: true, message: 'Campo requerido' }]}
      >
        <Select>
          <Option value="Siempre">Siempre</Option>
          <Option value="Casi siempre">Casi siempre</Option>
          <Option value="Algunas veces">Algunas veces</Option>
          <Option value="Muchas veces">Muchas veces</Option>
          <Option value="Sólo alguna vez">Sólo alguna vez</Option>
          <Option value="Nunca">Nunca</Option>
        </Select>
      </Form.Item>
    </Form>
  );
});

export default MyHealthQuestionnaire;
