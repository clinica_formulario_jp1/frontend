import React from "react";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import EncuestaPage from './pages/EncuestaPage/EncuestaPage';
import ListUser from "./pages/EncuestaPage/components/users";
import "./App.css";

const { Header, Content } = Layout;

function App() {
  return (
      <Layout>
        <Header>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1">
              <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/encuesta">Nueva Encuesta</Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/users">Users</Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: "24px", minHeight: "calc(100vh - 64px)" }}>
          <Routes>
            <Route path="/encuesta" element={<EncuestaPage />} />
            <Route path="/encuesta/:id" element={<EncuestaPage />} />
            <Route path="/users" element={<ListUser />} />
            <Route path="/" element={<Home />} />
          </Routes>
        </Content>
      </Layout>
  );
}

function Home() {
  return <h2>Home</h2>;
}

export default App;
